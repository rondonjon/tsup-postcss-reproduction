import { defineConfig } from "tsup";

const config = {
  clean: false,
  dts: false,
  entry: ["src/index.ts"],
  external: [],
  format: "iife",
  loader: {
    ".css": "text",
  },
  minify: true,
  noExternal: [/\.css$/],
  outExtension: () => ({ js: ".js" }),
  platform: "browser",
  shims: true,
  sourcemap: false,
  splitting: false,
  replaceNodeEnv: true,
};

export default defineConfig(config);
